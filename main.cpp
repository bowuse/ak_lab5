#include "func.h"
#include <stdlib.h>
#include <getopt.h>


int main (int argc, char *argv[]){

	const char* short_options = "hlvr";

	const struct option long_options[] = {
		{"help",no_argument,NULL,'h'},
		{"version",no_argument,NULL,'v'},
		{NULL,0,NULL,0}
	};

	int rez;
	int option_index;
	int count_commands = 0;

	bool was_help = false;
	bool was_version = false;
	bool was_error = false;

	while ((rez=getopt_long(argc,argv,short_options,
		long_options,&option_index))!=-1){

		switch(rez){
			case 'h': 
			{
                		if(!was_help){
                    			on_help();
                    			was_help = true;
                    			++count_commands;
                		}
				break;
			};
			case 'v': 
			{
				if(!was_version){
                    			on_version();
                    			was_version = true;
                    			++count_commands;
                		}
				break;
			};
			default:
			{
				was_error = true;
				break;
			};    
		};
	};
	if(was_error)
		on_error();
    	on_success(count_commands);
	return 0;
};
