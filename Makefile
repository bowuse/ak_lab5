all: ak_lab5

ak_lab5: func.o main.o
	g++ func.o main.o -o main

main.o: main.cpp
	g++ -c main.cpp

func.o: func.cpp
	g++ -c func.cpp

clean:
	rm -rf *.o main
