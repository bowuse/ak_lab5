#include <stdio.h>

void on_help()
{
	printf("We've known each other many years, but this is the first time you ever came to me for counsel or for help.\nI can't remember the last time that you invited me to your house for a cup of coffee, even though my wife is godmother to your only child.\nBut let's be frank here. You never wanted my friendship and, uh, you were afraid to be in my debt.\n");
}

void on_version()
{
	printf("Version v1972\n");
}

void on_error()
{
	printf("I didn't want to get into trouble\n");
}

void on_success(int count)
{
	printf("\nFound %i unique arguments\n", count);
}
