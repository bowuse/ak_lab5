# AK Lab5

Small C++ project with makefile

### Keys

| long name | short name  |
| ----------|-------------|
| --help    | -h          |
| --version | -v          |

### Example

```
./main -h
```

```
./main --version
```
